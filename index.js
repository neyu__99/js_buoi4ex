// ex1

document.getElementById('Ngayqua').onclick=function(){
    // input khai báo ngày tháng năm
    var Ngay=Number(document.getElementById('Ngay').value)
    var Thang=Number(document.getElementById('Thang').value)
    var Nam=Number(document.getElementById('Nam').value)
    // process
    if ((Ngay==1)&&((Thang==5)||(Thang==7)||(Thang==10)||(Thang==12))){Ngay=30;Thang-=1}
    else if ((Ngay==1)&&((Thang==2)||(Thang==4)||(Thang==6)||(Thang==8)||(Thang==9)||(Thang==11))){Ngay=31;Thang-=1}
    else if ((Ngay==1)&&(Thang==1)){Ngay=31;Thang=12;Nam-=1}
    else if ((Ngay==1)&&(Thang==3)){
        if (((Nam%4==0)&&(Nam%100!=0))||(Nam%400==0)){Ngay=29;Thang-=1}
        else {Ngay=28;Thang-=1}}
    else {Ngay-=1}
    // output giá trị ngày tháng năm mới
    document.getElementById('NgayMoi').innerHTML=` ${Ngay} / ${Thang} / ${Nam} `
}
document.getElementById('Ngaymai').onclick=function(){
    // input khai báo ngày tháng năm
    var Ngay=Number(document.getElementById('Ngay').value)
    var Thang=Number(document.getElementById('Thang').value)
    var Nam=Number(document.getElementById('Nam').value)
    // process
    if ((Ngay==31)&&((Thang==1)||(Thang==3)||(Thang==5)||(Thang==7)||(Thang==8)||(Thang==10))){Ngay=1;Thang+=1}
    else if ((Ngay==30)&&((Thang==4)||(Thang==6)||(Thang==9)||(Thang==11))){Ngay=1;Thang+=1}
    else if ((Ngay==31)&&(Thang==12)){Ngay=1;Thang=1;Nam+=1}
    else if ((((Nam%4==0)&&(Nam%100!=0))||(Nam%400==0))&&(Thang==2)&&(Ngay==29)){Ngay=1;Thang+=1}
    else if (!(((Nam%4==0)&&(Nam%100!=0))||(Nam%400==0))&&(Thang==2)&&(Ngay==28)) {Ngay=1;Thang+=1}
    else {Ngay+=1}
    // output giá trị ngày tháng năm mới
    document.getElementById('NgayMoi').innerHTML=` ${Ngay} / ${Thang} / ${Nam} `
}

// ex2
document.getElementById('Tinhngay').onclick=function(){
    var Thang=Number(document.getElementById('ex2Thang').value)
    var Nam=Number(document.getElementById('ex2Nam').value)
    var SoNgay=0
    if (((Thang==1)||(Thang==3)||(Thang==5)||(Thang==7)||(Thang==8)||(Thang==10)||(Thang==12))){SoNgay=31}
    else if ((Thang==4)||(Thang==6)||(Thang==9)||(Thang==11)){SoNgay=30}
    else if ((Thang==2)){
        if(((Nam%4==0)&&(Nam%100!=0))||(Nam%400==0)){SoNgay=29}
        else {SoNgay=28}}
    document.getElementById('SoNgay').innerHTML=`Tháng ${Thang} năm ${Nam} có ${SoNgay} ngày`
}

// ex3
document.getElementById('DocSo').onclick=function(){
    var So=Number(document.getElementById('ex3So').value)
    var Tram= Math.floor(So/100)
    var Chuc=Math.floor((So%100)/10)
    var Donvi=(So%100)%10
    if (Donvi==0){
        if (Chuc==0){document.getElementById('ChuSo').innerHTML=CachDoc(Tram)+ ' trăm '}
        else if (Chuc==1){document.getElementById('ChuSo').innerHTML=CachDoc(Tram)+ ' trăm mười'}
        else {document.getElementById('ChuSo').innerHTML=CachDoc(Tram)+ ' trăm ' + CachDoc(Chuc) +' mươi '}}
    else if (Chuc==0){document.getElementById('ChuSo').innerHTML=CachDoc(Tram)+ ' trăm lẻ ' + CachDoc(Donvi)}
    else {document.getElementById('ChuSo').innerHTML=CachDoc(Tram)+ ' trăm ' + CachDoc(Chuc) +' mươi ' + CachDoc(Donvi)}
}
function CachDoc(num) {
    switch(num){
        case 1: return 'một'
    }
    switch(num){
        case 2: return 'hai'
    }
    switch(num){
        case 3: return 'ba'
    }
    switch(num){
        case 4: return 'bốn'
    }
    switch(num){
        case 5: return 'năm'
    }
    switch(num){
        case 6: return 'sáu'
    }
    switch(num){
        case 7: return 'bảy'
    }
    switch(num){
        case 8: return 'tám'
    }
    switch(num){
        case 9: return 'chín'
    }
    switch(num){
        case 0: return 'lẻ'
    }
}

// ex4
document.getElementById('DuDoan').onclick=function(){
    var Sv1Ten = DomId('Sv1Ten')
    var Sv2Ten = DomId('Sv2Ten')
    var Sv3Ten = DomId('Sv3Ten')
    var Sv1x = DomId('Sv1x')*1
    var Sv2x = DomId('Sv2x')*1
    var Sv3x = DomId('Sv3x')*1
    var Sv1y = DomId('Sv1y')*1
    var Sv2y = DomId('Sv2y')*1
    var Sv3y = DomId('Sv3y')*1
    
    var Sv1kc= TinhKc(Sv1x,Sv1y)
    var Sv2kc= TinhKc(Sv2x,Sv2y)
    var Sv3kc= TinhKc(Sv3x,Sv3y)

    if ((Sv1kc>Sv2kc)&&(Sv1kc>Sv3kc)){document.getElementById('XaNhat').innerHTML= `Sinh viên xa nhất trường: ${Sv1Ten}`}
    else if ((Sv2kc>Sv1kc)&&(Sv2kc>Sv3kc)){document.getElementById('XaNhat').innerHTML= `Sinh viên xa nhất trường: ${Sv2Ten}`}
    else if ((Sv3kc>Sv2kc)&&(Sv3kc>Sv1kc)){document.getElementById('XaNhat').innerHTML= `Sinh viên xa nhất trường: ${Sv3Ten}`}
    else {document.getElementById('XaNhat').innerHTML= `Có nhiều sinh viên cách xa trường như nhau`}
}
function TinhKc(x,y){
    var THx = DomId('THx')*1
    var THy = DomId('THy')*1
    return Math.hypot((THx-x),(THy-y))
}
function DomId(Id) {
    return document.getElementById(Id).value
}

